#include "glExample/GlExampleInterface.hpp"
#include "GlExample.hpp"

namespace glExample {
    std::shared_ptr<GlExampleInterface> GlExampleInterface::GlExample() {
        return std::shared_ptr<GlExampleInterface>(new glExample::GlExample);
    }
}