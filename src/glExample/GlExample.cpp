#include "GlExample.hpp"
#include "MathHelper.h"
#include <sstream>
#include <iostream>
#include <vector>
#ifdef TARGET_PLATFORM_WEB
#include <SDL/SDL.h>
#endif


namespace glExample {
    GlExample::GlExample() :
            mWindowWidth(0),
            mWindowHeight(0),
            mDrawCount(0)
    {
#ifdef TARGET_PLATFORM_WEB
        //initialise context with SDL
        SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER);
        SDL_SetVideoMode(0, 0, 0, SDL_OPENGL);
#endif
        std::cout << "gl version in use: " << glGetString(GL_VERSION) << std::endl;
        // Vertex Shader source
        const std::string vs =
#if defined(TARGET_PLATFORM_LINUX)
            "#version 120\n"
#else
			"#version 100\n"
#endif
            "uniform mat4 uModelMatrix;"
            "uniform mat4 uViewMatrix;"
            "uniform mat4 uProjMatrix;"
            "attribute vec4 aPosition;"
            "attribute vec4 aColor;"
            "varying vec4 vColor;"
            "void main()"
            "{"
                "gl_Position = uProjMatrix * uViewMatrix * uModelMatrix * aPosition;"
                "vColor = aColor;"
            "}";


        // Fragment Shader source
        const std::string fs =
#if defined(TARGET_PLATFORM_LINUX)
			"#version 120\n"
#else
			"#version 100\n"
			"precision mediump float;"
#endif
            "varying vec4 vColor;"
            "void main()"
            "{"
                "gl_FragColor = vColor;"
            "}";


        // Set up the shader and its uniform/attribute locations.
        mProgram = compileProgram(vs, fs);
        mPositionAttribLocation = glGetAttribLocation(mProgram, "aPosition");
        mColorAttribLocation = glGetAttribLocation(mProgram, "aColor");
        mModelUniformLocation = glGetUniformLocation(mProgram, "uModelMatrix");
        mViewUniformLocation = glGetUniformLocation(mProgram, "uViewMatrix");
        mProjUniformLocation = glGetUniformLocation(mProgram, "uProjMatrix");

        // Then set up the cube geometry.
        GLfloat vertexPositions[] =
                {
                        -1.0f, -1.0f, -1.0f,
                        -1.0f, -1.0f,  1.0f,
                        -1.0f,  1.0f, -1.0f,
                        -1.0f,  1.0f,  1.0f,
                        1.0f, -1.0f, -1.0f,
                        1.0f, -1.0f,  1.0f,
                        1.0f,  1.0f, -1.0f,
                        1.0f,  1.0f,  1.0f,
                };

        glGenBuffers(1, &mVertexPositionBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, mVertexPositionBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPositions), vertexPositions, GL_STATIC_DRAW);

        GLfloat vertexColors[] =
                {
                        0.0f, 0.0f, 0.0f,
                        0.0f, 0.0f, 1.0f,
                        0.0f, 1.0f, 0.0f,
                        0.0f, 1.0f, 1.0f,
                        1.0f, 0.0f, 0.0f,
                        1.0f, 0.0f, 1.0f,
                        1.0f, 1.0f, 0.0f,
                        1.0f, 1.0f, 1.0f,
                };

        glGenBuffers(1, &mVertexColorBuffer);
        glBindBuffer(GL_ARRAY_BUFFER, mVertexColorBuffer);
        glBufferData(GL_ARRAY_BUFFER, sizeof(vertexColors), vertexColors, GL_STATIC_DRAW);

        short indices[] =
                {
                        0, 1, 2, // -x
                        1, 3, 2,

                        4, 6, 5, // +x
                        5, 6, 7,

                        0, 5, 1, // -y
                        0, 4, 5,

                        2, 7, 6, // +y
                        2, 3, 7,

                        0, 6, 4, // -z
                        0, 2, 6,

                        1, 7, 3, // +z
                        1, 5, 7,
                };

        glGenBuffers(1, &mIndexBuffer);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
    }


    void GlExample::render() {

        glEnable(GL_DEPTH_TEST);
        // blue background
        glClearColor(red, green, blue, 1.0f);

        // clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        if (mProgram == 0)
            return;
        glUseProgram(mProgram);

        glBindBuffer(GL_ARRAY_BUFFER, mVertexPositionBuffer);
#if defined(TARGET_PLATFORM_LINUX) || defined(TARGET_PLATFORM_MACOS)
        // https://stackoverflow.com/questions/24643027/opengl-invalid-operation-following-glenablevertexattribarray#24644675
        GLuint vaoId = 0;
        glGenVertexArrays(1, &vaoId);
        glBindVertexArray(vaoId);
#endif
        glEnableVertexAttribArray(mPositionAttribLocation);
        glVertexAttribPointer(mPositionAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

        glBindBuffer(GL_ARRAY_BUFFER, mVertexColorBuffer);
        glEnableVertexAttribArray(mColorAttribLocation);
        glVertexAttribPointer(mColorAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);

        MathHelper::Matrix4 modelMatrix = MathHelper::SimpleModelMatrix((float)mDrawCount / 50.0f);
        glUniformMatrix4fv(mModelUniformLocation, 1, GL_FALSE, &(modelMatrix.m[0][0]));

        MathHelper::Matrix4 viewMatrix = MathHelper::SimpleViewMatrix();
        glUniformMatrix4fv(mViewUniformLocation, 1, GL_FALSE, &(viewMatrix.m[0][0]));

        MathHelper::Matrix4 projectionMatrix = MathHelper::SimpleProjectionMatrix(float(mWindowWidth) / float(mWindowHeight));
        glUniformMatrix4fv(mProjUniformLocation, 1, GL_FALSE, &(projectionMatrix.m[0][0]));

        // Draw 36 indices: six faces, two triangles per face, 3 indices per triangle
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
        glDrawElements(GL_TRIANGLES, (6 * 2) * 3, GL_UNSIGNED_SHORT, 0);
        mDrawCount += 1;
    }


    void GlExample::setViewport(int32_t width, int32_t height) {
        // set the viewport
        // function is also called when user changes device orientation
        glViewport(0, 0, width, height);
        mWindowWidth = width;
        mWindowHeight = height;
    }

    void GlExample::setColor(float red, float green, float blue) {
            this->red = red;
            this->green = green;
            this->blue = blue;
    }

    GLuint GlExample::compileProgram(const std::string &vsSource, const std::string &fsSource) {
        GLuint program = glCreateProgram();

        if (program == 0)
        {
            throw std::runtime_error("[glExample] Program creation failed");
        }

        GLuint vs = compileShader(GL_VERTEX_SHADER, vsSource);
        GLuint fs = compileShader(GL_FRAGMENT_SHADER, fsSource);

        if (vs == 0 || fs == 0)
        {
            glDeleteShader(fs);
            glDeleteShader(vs);
            glDeleteProgram(program);
            return 0;
        }
        glAttachShader(program, vs);

        glAttachShader(program, fs);

        glDeleteShader(vs);
        glDeleteShader(fs);
        glLinkProgram(program);

        GLint linkStatus;
        glGetProgramiv(program, GL_LINK_STATUS, &linkStatus);

        if (linkStatus == 0)
        {
            GLint infoLogLength;
            glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLength);

            std::vector<GLchar> infoLog(infoLogLength);
            glGetProgramInfoLog(program, (GLsizei)infoLog.size(), NULL, infoLog.data());

            std::string errorMessage = std::string("[glExample] Program link failed: ");
            errorMessage += std::string(infoLog.begin(), infoLog.end());

            throw std::runtime_error(errorMessage);
        }

        return program;
    }

    GLuint GlExample::compileShader(GLenum type, const std::string &source) {
        GLuint shader = glCreateShader(type);

        const char *sourceArray[1] = { source.c_str() };
        glShaderSource(shader, 1, sourceArray, NULL);
        glCompileShader(shader);

        GLint compileResult;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &compileResult);

        if (compileResult == 0)
        {
            GLint infoLogLength;
            glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLength);

            std::vector<GLchar> infoLog(infoLogLength);
            glGetShaderInfoLog(shader, (GLsizei)infoLog.size(), NULL, infoLog.data());

            std::string errorMessage = std::string("[glExample] Shader compilation failed: ");
            errorMessage += std::string(infoLog.begin(), infoLog.end());

            throw std::runtime_error(errorMessage);
        }

        return shader;
    }

    GlExample::~GlExample() {
        if (mProgram != 0)
        {
            glDeleteProgram(mProgram);
            mProgram = 0;
        }

        if (mVertexPositionBuffer != 0)
        {
            glDeleteBuffers(1, &mVertexPositionBuffer);
            mVertexPositionBuffer = 0;
        }

        if (mVertexColorBuffer != 0)
        {
            glDeleteBuffers(1, &mVertexColorBuffer);
            mVertexColorBuffer = 0;
        }

        if (mIndexBuffer != 0)
        {
            glDeleteBuffers(1, &mIndexBuffer);
            mIndexBuffer = 0;
        }
    }

}



