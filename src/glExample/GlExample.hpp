#pragma once

#include "glExample/GlExampleInterface.hpp"

#include "config.h"

#ifdef TARGET_PLATFORM_IPHONE
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>
#endif
#ifdef TARGET_PLATFORM_JAVA
#include <GLES2/gl2.h>
#endif
#ifdef TARGET_PLATFORM_UWP
// Enable function definitions in the GL headers below
#define GL_GLEXT_PROTOTYPES

// OpenGL ES includes
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>

// EGL includes
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <EGL/eglplatform.h>

#include <angle_windowsstore.h>
#endif
#ifdef TARGET_PLATFORM_LINUX
#define GL_GLEXT_PROTOTYPES
#include <GL/gl.h>
#include <GL/glext.h>
#endif

#include <stdio.h>
#include <string>
#ifdef TARGET_PLATFORM_MACOS
#include <OpenGL/gl3.h>
#endif
#ifdef TARGET_PLATFORM_WEB
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#endif

namespace glExample {
    class GlExample : public GlExampleInterface {
    public:
        GlExample();
        ~GlExample();
        void render() override;
        void setViewport(int32_t width, int32_t height) override;
        void setColor(float red, float green, float blue) override;
    private:
        float red = 0.0f;
        float green = 0.0f;
        float blue = 0.0f;

        GLuint compileShader(GLenum type, const std::string &source);
        GLuint compileProgram(const std::string &vsSource, const std::string &fsSource);


        GLuint mProgram;
        GLsizei mWindowWidth;
        GLsizei mWindowHeight;

        GLint mPositionAttribLocation;
        GLint mColorAttribLocation;

        GLint mModelUniformLocation;
        GLint mViewUniformLocation;
        GLint mProjUniformLocation;

        GLuint mVertexPositionBuffer;
        GLuint mVertexColorBuffer;
        GLuint mIndexBuffer;

        int mDrawCount;
    };
}

