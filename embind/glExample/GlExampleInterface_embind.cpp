#include "glExample/GlExampleInterface.hpp"

#include <emscripten/bind.h>

using namespace emscripten;

EMSCRIPTEN_BINDINGS(glExample) {
        class_<glExample::GlExampleInterface>("GlExampleInterface")
                .smart_ptr<std::shared_ptr<glExample::GlExampleInterface>>("std::shared_ptr<glExample::GlExampleInterface>")
                .class_function("GlExample", &glExample::GlExampleInterface::GlExample)
                .function("render", &glExample::GlExampleInterface::render)
                .function("setViewport", &glExample::GlExampleInterface::setViewport)
                .function("setColor", &glExample::GlExampleInterface::setColor)
        ;
}