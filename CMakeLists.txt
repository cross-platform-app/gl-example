cmake_minimum_required(VERSION 3.0)

message(STATUS "\n========= Configuring gl-example =========")

# set up project variables
project(glExample
    VERSION
        1.0.0
    LANGUAGES
        CXX
)


# checking for required dependencies
find_package(Doxygen 1.8)


# setting up project config
set(generated_headers "${CMAKE_CURRENT_BINARY_DIR}/generated_headers")
## configure a header file to pass some of the CMake settings to the project
configure_file (
        "src/config.h.in"
        "${generated_headers}/config.h"
)

# add doxygen build support if doxygen is installed
if(DOXYGEN_FOUND)
    add_custom_target(
            glExample--doc
            ${DOXYGEN_EXECUTABLE} Doxyfile
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            COMMENT "Generating API documentation with Doxygen" VERBATIM
    )
endif(DOXYGEN_FOUND)


# Custom command to generate public Interfaces for C++, Java and Objective-C with Djinni
# for more information: https://github.com/dropbox/djinni
set(glExample--JAVA_PACKAGE org.cross_platform_app.glExample)
set(glExample--JAVA_PATH org/cross_platform_app/glExample PARENT_SCOPE)

message(STATUS "[djinni] Generating language bridges...")
execute_process(
    COMMAND
        bash djinni/src/run
        --java-out generated/java
        --java-package ${glExample--JAVA_PACKAGE}
        --ident-java-field mFooBar
        --cpp-out include/cpp/glExample
        --cpp-include-prefix glExample/
        --cpp-namespace glExample
        --jni-out generated/jni
        --ident-jni-class NativeFooBar
        --jni-include-cpp-prefix glExample/
        --objc-out include/objc/glExample
        --objc-type-prefix GE
        --objcpp-out generated/objcpp
        --objcpp-include-cpp-prefix glExample/
        --objcpp-include-objc-prefix glExample/
        --idl glExample.djinni
    WORKING_DIRECTORY
        ${CMAKE_CURRENT_SOURCE_DIR}
    RESULT_VARIABLE DJINNI_RESULT
)

if(${DJINNI_RESULT} MATCHES 0)
    message(STATUS "[djinni] Generation successful!")
else()
    message(SEND_ERROR "[djinni] Generation failed with result: '${DJINNI_RESULT}'")
endif()


# setting up c++library
message(STATUS ">>> Configuring C++ Library Target")

add_library(glExample--Cpp STATIC
        src/glExample/GlExampleInterface.cpp
        src/glExample/GlExample.cpp
)

add_library(glExample::Cpp ALIAS glExample--Cpp)



target_include_directories(glExample--Cpp
    PUBLIC
        include/cpp
    PRIVATE
        "$<BUILD_INTERFACE:${generated_headers}>"

)

target_compile_features(glExample--Cpp
    PRIVATE
        cxx_nonstatic_member_init
        cxx_override
)

if(${TARGET_PLATFORM} MATCHES LINUX)

    find_package(OpenGL REQUIRED)

    target_link_libraries(glExample--Cpp
        PUBLIC
            OpenGL::GL
    )
endif()

if(${TARGET_PLATFORM} MATCHES "MACOS" OR ${TARGET_PLATFORM} MATCHES "IPHONE")
    message(STATUS ">>> Configuring Library Target for MacOS and iPhone (Objective-C) Language Bindings")

    file(GLOB glExample--ObjC_SOURCES
            ${CMAKE_CURRENT_SOURCE_DIR}/generated/objcpp/*.mm
            ${CMAKE_CURRENT_SOURCE_DIR}/djinni/support-lib/objc/*.mm
    )

    add_library(glExample--ObjC STATIC
            ${glExample--ObjC_SOURCES}
    )

    add_library(glExample::ObjC ALIAS glExample--ObjC)

    target_link_libraries(glExample--ObjC
        PRIVATE
            glExample::Cpp
    )

    target_include_directories(glExample--ObjC
        PUBLIC
            include/objc
        PRIVATE
            djinni/support-lib/objc
    )

    target_compile_features(glExample--ObjC
        PRIVATE
            cxx_alias_templates
    )

    set_target_properties(glExample--ObjC
        PROPERTIES
            COMPILE_FLAGS "-x objective-c++ -fobjc-arc"
    )
endif()


if(${TARGET_PLATFORM} MATCHES "JAVA")
    message(STATUS ">>> Configuring Library Target for JNI Language Bindings")

    # setting up jni library
    file(GLOB glExample--JNI_SOURCES
            ${CMAKE_CURRENT_SOURCE_DIR}/generated/jni/*.cpp
            ${CMAKE_CURRENT_SOURCE_DIR}/djinni/support-lib/jni/*.cpp
    )

    file(GLOB glExample--JAVA_SOURCES
            ${CMAKE_CURRENT_SOURCE_DIR}/generated/java/*.java
    )
    set(glExample--JAVA_SOURCES ${glExample--JAVA_SOURCES} PARENT_SCOPE)

    add_library(glExample--JNI SHARED
            ${glExample--JNI_SOURCES}
    )

    add_library(glExample::JNI ALIAS glExample--JNI)


    target_link_libraries(glExample--JNI
        glExample::Cpp
        GLESv2
        EGL
    )


    target_include_directories(glExample--JNI
        PRIVATE
            djinni/support-lib/jni/
    )

    set_target_properties( glExample--JNI
        PROPERTIES
            DEBUG_POSTFIX ""
    )

endif()


if(${TARGET_PLATFORM} MATCHES WEB)
    add_executable(glExample--emscripten
            embind/glExample/GlExampleInterface_embind.cpp
    )
    target_link_libraries(glExample--emscripten
            glExample::Cpp
    )
    target_compile_features(glExample--emscripten
        PRIVATE
            cxx_alias_templates
    )
    set_target_properties(glExample--emscripten
        PROPERTIES
            LINK_FLAGS "-s DEMANGLE_SUPPORT=1 --bind -s MODULARIZE=1 -s EMTERPRETIFY=1 -s EMTERPRETIFY_ASYNC=1 -s EXPORT_NAME=\"'GlExample'\""
    )
endif()
